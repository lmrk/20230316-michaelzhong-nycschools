package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

data class Score(
    @SerializedName("dbn")
    val dbn: String? = null,
    @SerializedName("school_name")
    val name: String? = null,
    @SerializedName("sat_math_avg_score")
    val mathScore: String? = null,
    @SerializedName("sat_critical_reading_avg_score")
    val readScore: String? = null,
    @SerializedName("sat_writing_avg_score")
    val writingScore: String? = null
)
