package com.example.nycschools.model

class NullResponseBodyException(message: String) : Exception(message)
class FailureCallException(message: String? = "Error in connection") : Exception(message)