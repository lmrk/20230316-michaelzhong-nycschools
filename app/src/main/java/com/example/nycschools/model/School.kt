package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

data class School(
    @SerializedName("dbn")
    val dbn: String? = null,
    @SerializedName("school_name")
    val name: String? = null,
    @SerializedName("overview_paragraph")
    val overview: String? = null,
    @SerializedName("phone_number")
    val phone: String? = null,
    @SerializedName("website")
    val website: String? = null,
    @SerializedName("location")
    val address: String? = null,
    @SerializedName("school_email")
    val email: String? = null
)
