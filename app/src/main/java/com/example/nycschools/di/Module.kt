package com.example.nycschools.di

import com.example.nycschools.service.SchoolsRepo
import com.example.nycschools.service.SchoolsRepoImpl
import com.example.nycschools.service.ServiceApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Module {

    val repository: SchoolsRepo by lazy {
        SchoolsRepoImpl(serviceApi)
    }

    private val serviceApi: ServiceApi by lazy {
        Retrofit.Builder()
            .baseUrl(ServiceApi.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okhttpClient)
            .build()
            .create(ServiceApi::class.java)
    }

    private val okhttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
            .build()
    }
}