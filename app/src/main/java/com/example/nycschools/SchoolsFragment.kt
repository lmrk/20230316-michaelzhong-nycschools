package com.example.nycschools

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nycschools.adapter.ItemsAdapter
import com.example.nycschools.databinding.FragmentSchoolsBinding
import com.example.nycschools.viewmodel.NycViewModel
import com.example.nycschools.viewmodel.ViewState

class SchoolsFragment : Fragment() {

    private val binding by lazy {
        FragmentSchoolsBinding.inflate(layoutInflater)
    }

    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[NycViewModel::class.java]
    }

    private val mAdapter by lazy {
        ItemsAdapter {
            viewModel.school = it
            findNavController().navigate(R.id.action_ListFragment_to_DetailsFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding.schoolsRecycler.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapter
        }

        viewModel.nycSchools.observe(viewLifecycleOwner) {
            when(it) {
                is ViewState.LOADING -> {
                    binding.schoolsRecycler.visibility = View.GONE
                    binding.loadNyc.visibility = View.VISIBLE
                }
                is ViewState.SCHOOLS -> {
                    binding.schoolsRecycler.visibility = View.VISIBLE
                    binding.loadNyc.visibility = View.GONE

                    mAdapter.updateNewSchools(it.schools)
                }
                is ViewState.ERROR -> {
                    binding.schoolsRecycler.visibility = View.GONE
                    binding.loadNyc.visibility = View.GONE

                    AlertDialog.Builder(requireActivity())
                        .setTitle("We are having issues")
                        .setMessage(it.error.localizedMessage)
                        .setNegativeButton("DISMISS") { d, _ ->
                            d.dismiss()
                        }
                        .setPositiveButton("RETRY") { d, _ ->
                            viewModel.getAllSchools()
                            d.dismiss()
                        }
                        .create()
                        .show()
                }
                else -> {

                }
            }
        }

        return binding.root
    }
}