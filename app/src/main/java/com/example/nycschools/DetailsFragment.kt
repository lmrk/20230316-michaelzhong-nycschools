package com.example.nycschools

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.nycschools.databinding.FragmentDetailsBinding
import com.example.nycschools.model.School
import com.example.nycschools.model.Score
import com.example.nycschools.viewmodel.NycViewModel
import com.example.nycschools.viewmodel.ViewState

class DetailsFragment : Fragment() {

    private val binding by lazy {
        FragmentDetailsBinding.inflate(layoutInflater)
    }

    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[NycViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        initSchoolDetails(viewModel.school)

        viewModel.nycScores.observe(viewLifecycleOwner) {
            when(it) {
                is ViewState.LOADING -> {}
                is ViewState.SCORES -> {
                    initScoresView(it.score)
                }
                is ViewState.ERROR -> {
                    AlertDialog.Builder(requireActivity())
                        .setTitle("We are having issues")
                        .setMessage(it.error.localizedMessage)
                        .setNegativeButton("DISMISS") { d, _ ->
                            d.dismiss()
                        }
                        .create()
                        .show()
                }
                else -> {}
            }
        }

        viewModel.filterScoresById()

        return binding.root
    }

    private fun initScoresView(score: Score?) {
        score?.let {
            binding.mathScore.text = String.format("Math Score: ${it.mathScore}")
            binding.readScore.text = String.format("Reading Score: ${it.readScore}")
            binding.writeScore.text = String.format("Writing Score: ${it.writingScore}")
        } ?: let {
            binding.mathScore.text = String.format("NOT AVAILABLE")
            binding.readScore.text = String.format("NOT AVAILABLE")
            binding.writeScore.text = String.format("NOT AVAILABLE")
        }
    }

    private fun initSchoolDetails(school: School?) {
        school?.let {
            binding.adddressSchool.text = school.address
            binding.emailSchool.text = school.email
            binding.nameSchool.text = school.name
            binding.overviewSchool.text = school.overview
            binding.phoneSchool.text = school.phone
            binding.websiteSchool.text = school.website
        } ?: let {
            AlertDialog.Builder(requireActivity())
                .setTitle("We are having issues")
                .setMessage("Please select a school")
                .setNegativeButton("DISMISS") { d, _ ->
                    findNavController().navigate(R.id.action_DetailsFragment_to_ListFragment)
                    d.dismiss()
                }
                .create()
                .show()
        }
    }
}