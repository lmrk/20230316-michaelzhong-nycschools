package com.example.nycschools.viewmodel

import com.example.nycschools.model.School
import com.example.nycschools.model.Score

sealed class ViewState {
    object LOADING : ViewState()
    data class SCHOOLS(val schools: List<School>) : ViewState()
    data class SCORES(val scores: List<Score>? = null, val score: Score? = null) : ViewState()
    data class ERROR(val error: Exception) : ViewState()
}
