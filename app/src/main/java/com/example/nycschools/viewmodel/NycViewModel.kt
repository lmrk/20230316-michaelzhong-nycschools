package com.example.nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.di.Module
import com.example.nycschools.model.School
import com.example.nycschools.model.Score
import com.example.nycschools.service.SchoolsRepo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NycViewModel(
    private val schoolsRepo: SchoolsRepo = Module.repository,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    init {
        getAllSchools()
        getScores()
    }

    var school: School? = null
    private var scores: List<Score> = emptyList()

    private val _nycSchools: MutableLiveData<ViewState> = MutableLiveData(ViewState.LOADING)
    val nycSchools: LiveData<ViewState> get() = _nycSchools

    private val _nycScores: MutableLiveData<ViewState> = MutableLiveData(ViewState.LOADING)
    val nycScores: LiveData<ViewState> get() = _nycScores

    fun getAllSchools() {
        viewModelScope.launch(ioDispatcher) {
            schoolsRepo.getSchools().collect {
                _nycSchools.postValue(it)
            }
        }
    }

    fun getScores() {
            viewModelScope.launch(ioDispatcher) {
                schoolsRepo.getScores().collect {
                    if (it is ViewState.SCORES) {
                        scores = it.scores ?: emptyList()
                    } else {
                        _nycScores.postValue(it)
                    }
                }
            }
    }

    fun filterScoresById() {
        school?.let { mSchool ->
            mSchool.dbn?.let { id ->
                _nycScores.postValue(ViewState.SCORES(null, scores.firstOrNull { it.dbn == id }))
            }
        }
    }
}